﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Office.Tools.Ribbon;
using Outlook = Microsoft.Office.Interop.Outlook;
using Office = Microsoft.Office.Core;
using System.Diagnostics;
using System.Net;
using System.Collections.Specialized;
using System.Net.Security;
using System.Text.RegularExpressions;

namespace PosForOutlook
{
    public partial class AddToPosMenu
    {
        /// <summary>
        /// Main Button
        /// </summary>
        Microsoft.Office.Tools.Ribbon.RibbonButton mainButton;
        /// <summary>
        /// Mail Object Item
        /// </summary>
        Outlook.MailItem mailItem;
        /// <summary>
        /// POS Domain
        /// http://pos.targetmedia.eu
        /// http://personal-org.vietnamcubator.com
        /// 
        /// http://posys.local
        /// </summary>
        private string HostUrl = "https://pos.digiwallet.nl";
        /// <summary>
        /// Menu Load event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AddToPosMenu_Load(object sender, RibbonUIEventArgs e)
        {
            // Do nothings
        }
        /// <summary>
        /// Click add to POS event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAddToPos_Click(object sender, RibbonControlEventArgs e)
        {
            mainButton = (Microsoft.Office.Tools.Ribbon.RibbonButton)sender;
            mainButton.Enabled = false;
            mailItem = GetMailItem(e);
            if(mailItem == null)
            {
                System.Windows.Forms.MessageBox.Show("Please select an email to use this feature.", "Pos for Outlook", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error, System.Windows.Forms.MessageBoxDefaultButton.Button1);
                return;
            }
            System.ComponentModel.BackgroundWorker bgWork = new System.ComponentModel.BackgroundWorker();
            bgWork.DoWork += BgWork_DoWork;
            bgWork.RunWorkerCompleted += BgWork_RunWorkerCompleted;
            bgWork.RunWorkerAsync();            
        }

        /// <summary>
        /// Upload Email content and start page
        /// </summary>
        private void ProcessEmail()
        {
            // Post data to server first
            string URI = HostUrl + "/outlook/add?target=msapp";

            using (WebClient wc = new WebClient())
            {
                try
                {
                    if (InitiateSSLTrust())
                    {
                        string postID = Guid.NewGuid().ToString();
                        NameValueCollection mailData = new NameValueCollection();
                        mailData.Add("addins_service_id", "MsOutlook");
                        mailData.Add("app_id", postID);
                        mailData.Add("addins_title", mailItem.Subject);
                        mailData.Add("addins_content", mailItem.HTMLBody);
                        mailData.Add("info[id]", mailItem.EntryID);
                        TimeSpan epochTicks = new TimeSpan(new DateTime(1970, 1, 1).Ticks);
                        TimeSpan unixTicks = new TimeSpan(mailItem.ReceivedTime.AddHours(-1).AddMinutes(1).Ticks) - epochTicks;
                        double unixTime = unixTicks.TotalMilliseconds;
                        mailData.Add("info[time]", unixTime.ToString());

                        mailData.Add("info[from][email]", mailItem.SenderEmailAddress);
                        mailData.Add("info[from][name]", mailItem.SenderName);
                        
                        try
                        {
                            var profileEmail = mailItem.Session.CurrentUser.AddressEntry.GetExchangeUser().PrimarySmtpAddress;
                            mailData.Add("info[sender_email]", profileEmail);
                        }
                        catch
                        {
                            mailData.Add("info[sender_email]", mailItem.Session.CurrentUser.Address);
                            // Do nothings
                        }

                        int indexTo = 0;
                        int indexCC = 0;
                        string cc = mailItem.CC;
                        foreach (Outlook.Recipient recevier in mailItem.Recipients)
                        {
                            try
                            {
                                string email = "Unknown";
                                try
                                {
                                    email = recevier.AddressEntry.GetExchangeUser().PrimarySmtpAddress;
                                }
                                catch
                                {
                                    // Do nothings
                                    email = recevier.Address;
                                }
                                
                                if (recevier.Type == (int)Outlook.OlMailRecipientType.olTo)
                                {
                                    mailData.Add(string.Format("info[to][{0}][name]", indexTo), recevier.Name);
                                    mailData.Add(string.Format("info[to][{0}][email]", indexTo), email);
                                    indexTo++;
                                }
                                if (recevier.Type == (int)Outlook.OlMailRecipientType.olCC)
                                {
                                    mailData.Add(string.Format("info[cc][{0}][name]", indexCC), recevier.Name);
                                    mailData.Add(string.Format("info[cc][{0}][email]", indexCC), email);
                                    indexCC++;
                                }
                            }
                            catch
                            {
                                // Do nothings
                            }
                        }
                        // Inline attachment detector
                        Regex reg = new Regex(@"<img .+?>", RegexOptions.Singleline | RegexOptions.CultureInvariant | RegexOptions.IgnoreCase);
                        MatchCollection matches = reg.Matches(mailItem.HTMLBody);
                        // Get attachments
                        int index = 0;
                        foreach(Outlook.Attachment item in mailItem.Attachments)
                        {
                            bool isMatch = matches
                                           .OfType<Match>()
                                           .Select(m => m.Value)
                                           .Where(s => s.IndexOf("cid:" + item.FileName, StringComparison.InvariantCultureIgnoreCase) >= 0)
                                           .Any();
                            if (isMatch) continue;

                            if (item.Size > 0)
                            {
                                mailData.Add(string.Format("attachments[{0}][name]", index), item.FileName);
                                mailData.Add(string.Format("attachments[{0}][size]", index), item.Size.ToString());
                                mailData.Add(string.Format("attachments[{0}][id]", index), Guid.NewGuid().ToString());

                                string savePath = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
                                savePath = System.IO.Path.Combine(savePath, Guid.NewGuid().ToString());
                                if (!System.IO.Directory.Exists(savePath))
                                {
                                    System.IO.Directory.CreateDirectory(savePath);
                                }
                                savePath = System.IO.Path.Combine(savePath, item.FileName);
                                item.SaveAsFile(savePath);
                                string type = GetContentType(savePath);
                                mailData.Add(string.Format("attachments[{0}][attachmentType]", index), type);
                                mailData.Add(string.Format("attachments[{0}][contentType]", index), type);
                                Byte[] bytes = System.IO.File.ReadAllBytes(savePath);
                                String file = Convert.ToBase64String(bytes);
                                mailData.Add(string.Format("attachments[{0}][fileData]", index), file);
                                index++;
                            }
                        }
                        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
                        wc.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
                        byte[] result = wc.UploadValues(URI, "POST", mailData);
                        string dataResult = Encoding.UTF8.GetString(result);
                        if (dataResult != string.Empty && dataResult != "0")
                        {
                            // Open browser
                            Process browser = new Process();
                            browser.StartInfo.UseShellExecute = true;
                            browser.StartInfo.FileName = HostUrl + "/outlook/index?target=msapp&token=" + dataResult;
                            browser.Start();
                        }
                        else
                        {
                            System.Windows.Forms.MessageBox.Show("Can't connect to your POS, please try again later.", "Pos for Outlook", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error, System.Windows.Forms.MessageBoxDefaultButton.Button1);
                            return;
                        }
                    }
                }
                catch (Exception ex)
                {
                    System.Windows.Forms.MessageBox.Show(ex.Message, "Pos for Outlook", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error, System.Windows.Forms.MessageBoxDefaultButton.Button1);
                    return;
                }

            }
        }
        private string GetContentType(string fileName)
        {
            string strcontentType = "application/octetstream";
            string ext = System.IO.Path.GetExtension(fileName).ToLower();
            Microsoft.Win32.RegistryKey registryKey = Microsoft.Win32.Registry.ClassesRoot.OpenSubKey(ext);
            if (registryKey != null && registryKey.GetValue("Content Type") != null)
                strcontentType = registryKey.GetValue("Content Type").ToString();
            return strcontentType;
        }
        /// <summary>
        /// Update Button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BgWork_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            mainButton.Enabled = true;
        }
        /// <summary>
        /// Run Process Email
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BgWork_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            ProcessEmail();
        }
        /// <summary>
        /// Allow HTTPS connection
        /// </summary>
        /// <returns></returns>
        public static bool InitiateSSLTrust()
        {
            try
            {
                //Change SSL checks so that all checks pass
                ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(delegate { return true; });
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message, "Error", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error, System.Windows.Forms.MessageBoxDefaultButton.Button1);
                return false;
            }
            return true;
        }
        /// <summary>
        /// Get Outlook mail Item
        /// </summary>
        /// <param name="e"></param>
        /// <returns></returns>
        public Outlook.MailItem GetMailItem(RibbonControlEventArgs e)
        {
            // Check to see if an item is selected in explorer or we are in inspector.
            if (e.Control.Context is Outlook.Inspector)
            {
                Outlook.Inspector inspector = (Outlook.Inspector)e.Control.Context;

                if (inspector.CurrentItem is Outlook.MailItem)
                {
                    return inspector.CurrentItem as Outlook.MailItem;
                }
            }

            if (e.Control.Context is Outlook.Explorer)
            {
                Outlook.Explorer explorer = (Outlook.Explorer)e.Control.Context;

                Outlook.Selection selectedItems = explorer.Selection;
                if (selectedItems.Count != 1)
                {
                    return null;
                }

                if (selectedItems[1] is Outlook.MailItem)
                {
                    return selectedItems[1] as Outlook.MailItem;
                }
            }

            return null;
        }
    }
}
