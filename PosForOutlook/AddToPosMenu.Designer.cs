﻿namespace PosForOutlook
{
    partial class AddToPosMenu : Microsoft.Office.Tools.Ribbon.RibbonBase
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        public AddToPosMenu()
            : base(Globals.Factory.GetRibbonFactory())
        {
            InitializeComponent();
        }

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tab1 = this.Factory.CreateRibbonTab();
            this.targetMediaMenu = this.Factory.CreateRibbonGroup();
            this.btnAddToPos = this.Factory.CreateRibbonButton();
            this.tab1.SuspendLayout();
            this.targetMediaMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // tab1
            // 
            this.tab1.ControlId.ControlIdType = Microsoft.Office.Tools.Ribbon.RibbonControlIdType.Office;
            this.tab1.Groups.Add(this.targetMediaMenu);
            this.tab1.Label = "TabAddIns";
            this.tab1.Name = "tab1";
            // 
            // targetMediaMenu
            // 
            this.targetMediaMenu.Items.Add(this.btnAddToPos);
            this.targetMediaMenu.Label = "TargetMedia";
            this.targetMediaMenu.Name = "targetMediaMenu";
            // 
            // btnAddToPos
            // 
            this.btnAddToPos.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.btnAddToPos.Description = "Add your email to POS as Task/Bug/Backlog/Wish";
            this.btnAddToPos.Image = global::PosForOutlook.Properties.Resources.logo;
            this.btnAddToPos.Label = "Add To POS";
            this.btnAddToPos.Name = "btnAddToPos";
            this.btnAddToPos.ScreenTip = "Add your email to POS as Task/Bug/Backlog/Wish";
            this.btnAddToPos.ShowImage = true;
            this.btnAddToPos.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.btnAddToPos_Click);
            // 
            // AddToPosMenu
            // 
            this.Name = "AddToPosMenu";
            this.RibbonType = "Microsoft.Outlook.Explorer, Microsoft.Outlook.Mail.Read, Microsoft.Outlook.Post.R" +
    "ead, Microsoft.Outlook.Response.Read, Microsoft.Outlook.Sharing.Read";
            this.Tabs.Add(this.tab1);
            this.Load += new Microsoft.Office.Tools.Ribbon.RibbonUIEventHandler(this.AddToPosMenu_Load);
            this.tab1.ResumeLayout(false);
            this.tab1.PerformLayout();
            this.targetMediaMenu.ResumeLayout(false);
            this.targetMediaMenu.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        internal Microsoft.Office.Tools.Ribbon.RibbonTab tab1;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup targetMediaMenu;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton btnAddToPos;
    }

    partial class ThisRibbonCollection
    {
        internal AddToPosMenu AddToPosMenu
        {
            get { return this.GetRibbon<AddToPosMenu>(); }
        }
    }
}
